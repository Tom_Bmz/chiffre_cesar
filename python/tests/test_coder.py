import unittest
from chiffre_cesar import Coder

class TestCoder(unittest.TestCase):

    def test_Coder(self):
        self.assertEqual(Coder('A'), 'I')
        self.assertEqual(Coder('a'), 'i')
        self.assertEqual(Coder('x'), '\x80')
        self.assertEqual(Coder('abc'), 'ijk')


  