
import unittest
from chiffre_cesar import Decoder

class TestDecoder(unittest.TestCase):



    def test_Decoder(self):
        self.assertEqual(Decoder('I'), 'A')
        self.assertEqual(Decoder('i'), 'a')
        self.assertEqual(Decoder('\x80'), 'x')
        self.assertEqual(Decoder('ijk'), 'abc')